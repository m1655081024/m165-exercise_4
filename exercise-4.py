import datetime
import psutil
from pymongo import MongoClient
import matplotlib.pyplot as plt
import numpy as np

connectionString = "mongodb://localhost:27017/"
client = MongoClient(connectionString)


class Power:
    def __init__(self, cpu, ram_total, ram_used, timestamp=None):
        self.cpu = cpu
        self.ram_total = ram_total
        self.ram_used = ram_used
        if timestamp is None:
            self.timestamp = datetime.datetime.now()
        else:
            self.timestamp = timestamp


#example data
cpu_values = [15, 5, 50, 30, 20,5]
ram_values = [4000, 4200, 4100, 3800, 3900, 3800]

plt.style.use('_mpl-gallery')

fig, ax = plt.subplots()

ax.plot(cpu_values, color='red', label='CPU Usage in %')

ax.plot(ram_values, color='blue', label='RAM Usage in MB')

ax.set_xlabel('Time')
ax.set_ylabel('Usage')
ax.legend()

plt.show()